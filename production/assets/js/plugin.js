/* ----------------------------------------------------------------------------
URL: http://bitbucket.org/mortar/inview
Author: Daniel Charman (daniel@blackmaze.com.au)
Created: 2014-05-24
---------------------------------------------------------------------------- */
(function( $ ) {
 
    $.fn.inview = function(options) {

        var defaults = {
            debug:      false
        };
     
        var settings = $.extend( {}, defaults, options );

        var obj = $(this);
        var windowTop = $(window).scrollTop() + 65;
        var windowBottom = windowTop + $(window).height();
        var objStartPos = obj.offset().top;
        var objEndPos = (obj.offset().top + obj.outerHeight());

        if(settings.debug) console.log(windowTop, objStartPos, windowBottom, objEndPos);

        return ((windowTop >= objStartPos && windowTop <= objEndPos) || (windowBottom >= objStartPos && windowBottom <= objEndPos)) ? true : false;

    };
 
}( jQuery ));